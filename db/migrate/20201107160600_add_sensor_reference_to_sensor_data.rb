class AddSensorReferenceToSensorData < ActiveRecord::Migration[5.1]
  def change
    execute <<-SQL
      INSERT INTO sensors(id, created_at, updated_at)
        SELECT sd.sensor_id id, MIN(sd.created_at) created_at, MAX(sd.updated_at) updated_at
        FROM sensor_data sd
        GROUP BY sd.sensor_id
    SQL
    add_foreign_key :sensor_data, :sensors, index: true
    remove_column :sensor_data, :time
  end
end
