class CreateSensors < ActiveRecord::Migration[5.1]
  def change
    create_table :sensors, id: :string do |t|
      t.references :user, foreign_key: true, type: :uuid

      t.timestamps
    end
  end
end
