class AddStateToSensorData < ActiveRecord::Migration[5.1]
  def up
    execute <<-SQL
      CREATE TYPE state_enum AS ENUM ('auto', 'closed', 'open', 'force_closed', 'force_open');
    SQL
    add_column :sensor_data, :state, :state_enum
    execute <<-SQL
      UPDATE sensor_data SET state='closed'
      WHERE angle = 0
    SQL
    execute <<-SQL
      UPDATE sensor_data SET state='open'
      WHERE angle != 0
    SQL
    remove_column :sensor_data, :angle
  end

  def down
    add_column :sensor_data, :angle, :integer
    execute <<-SQL
      UPDATE sensor_data SET angle = 180
      WHERE state='open' or state='force_open' or state='auto'
    SQL
    execute <<-SQL
      UPDATE sensor_data SET angle = 0
      WHERE state='closed' or state = 'force_closed'
    SQL
    remove_column :sensor_data, :state
    execute <<-SQL
      DROP TYPE state_enum;
    SQL
  end
end
