class ChangeSensorsToSensorData < ActiveRecord::Migration[5.1]
  def change
    rename_table :sensors, :sensor_data
  end
end
