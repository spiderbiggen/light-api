class CreateOldSensors < ActiveRecord::Migration[5.1]
  def change
    create_table :sensors, id: :uuid do |t|
      t.string :sensor_id
      t.integer :light
      t.integer :angle
      t.timestamp :time

      t.timestamps
    end
  end
end
