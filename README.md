# README

## Setup

Make sure to install ruby 2.7 for the best experience. Next make sure to install the native postgres client libraries.
Run `bundle install` to install all dependencies

## Configuration

Some environment variables should be set to configure the api to use your postgres database. The following environment
variables are available. These can be set by adding a `.env` file to the root of the project (use at your own risk in production).

| VARIABLE                      | DESCRIPTION                                                       |
|-------------------------------|-------------------------------------------------------------------|
| `RAILS_ENV`                   | Tells rails in what mode to run the server. can be `development`, `test`, `production` |
| `LIGHT_API_DATABASE_PASSWORD` | The password for the database                                     |
| `LIGHT_API_DATABASE_HOST`     | The host for the database. Can include port like `127.0.0.1:5432` |
| `SECRET_KEY_BASE`             | Base secret key for internal signing                              |
| `SECRET_KEY_JWT`              | Secret key for signing jwt tokens                                 |

Database user and name are dependant on environment

| ENVIRONMENT   | USER            | DATABASE NAME           |
|---------------|-----------------|-------------------------|
| `development` | light-api       | light-api_development   |
| `test`        | light-api_test  | light-api_test          |
| `production ` | light-api       | light-api_production    |

## Development

Run `rake db:migrate` to make sure that all migrations have been applied.  
Start Server with `rails server` or `rails server -b 0.0.0.0` if you want to access the application remotely

