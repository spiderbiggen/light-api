FROM spiderbiggen/ruby:latest
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client
RUN gem update --system
RUN mkdir /light
WORKDIR /light
COPY Gemfile /light/Gemfile
COPY Gemfile.lock /light/Gemfile.lock
RUN bundle install
COPY . /light

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]
