Rails.application.routes.draw do
  post 'users/refresh', to: 'refresh#refresh'
  devise_for :users, defaults: { format: :json }

  resources :sensor_data, only: %i[index]
  scope '/sensors', defaults: { format: :json } do
    get '/', to: 'sensor#get'
    post '/:id/claim', to: 'sensor#claim'
    post '/:id/state', to: 'sensor#set_state'
    delete '/:id/claim', to: 'sensor#unclaim'
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
