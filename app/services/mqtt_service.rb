# frozen_string_literal: true

# Manages MQTT connection.
class MqttService
  attr_reader :client

  def initialize
    @client = MQTT::Client.new(
      host: ENV.fetch('MQTT_HOST') { 'mqtt.spiderbiggen.com' },
      port: (ENV.fetch('MQTT_PORT') { 1883 }).to_i
    )
  end

  def connect
    @client.connect
  end

  def close
    @client.disconnect
  end

  def connect_to_broker
    @client.connect
    subscribe
    receive
  end

  def subscribe
    # only subscribe to the data channel of the iot devices for now
    @client.subscribe('iot/device/+/data')
  end

  def send(topic, message, retain=false, qos=0)
    @client.publish(topic, message, retain, qos)
  end

  def receive
    Thread.new do
      @client.get do |topic, message|
        topic.match %r{iot/device/(0x[0-9A-F]+)(?:/(.*))?$} do |m|
          process_message(m[1], m[2], message)
        end
      end
    end
  end

  private

  def process_message(guid, path, message)
    case path
    when 'data'
      process_data guid, message
    else
      Rails.logger.debug "#{guid}/#{path}: #{message}"
    end
  end

  # custom packed data to be small and easy to decode
  # 2 bytes for the 10 bit analog light value
  # 1 byte for the state of the device
  def process_data(guid, message)
    bytes = message.unpack('C*')
    light = bytes[0] << 8 | bytes[1]
    state = bytes[2].to_i

    Rails.logger.debug "#{guid} light: #{light}, state: #{state}/#{map_state(state)}"
    sensor = Sensor.find_or_create_by(id: guid)
    SensorData.create(sensor: sensor, light: light, state: map_state(state))
  end

  def map_state(state)
    case state
    when 0
      :auto
    when 1
      :closed
    when 2
      :open
    when 3
      :force_closed
    when 4
      :force_open
    else
      :auto
    end
  end
end
