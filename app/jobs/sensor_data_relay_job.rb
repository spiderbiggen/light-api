class SensorDataRelayJob < ApplicationJob
  def perform(sensor_data)
    user_id = sensor_data.sensor&.user&.id
    return if user_id == nil

    ActionCable.server.broadcast "sensor_data_#{user_id}", sensor_data: sensor_data
  end
end
