# frozen_string_literal: true

class SensorDataController < ApplicationController

  # GET /sensor_data
  # Only fetches data that is owned by the current user
  def index
    user = current_user
    offset = between params[:offset], 0, 0
    limit = between params[:limit], 10, 1, 10000

    user.sensors.all.each do |sensor|
      puts sensor
    end

    @sensor_data = SensorData.filter(filtering_params params)
                       .joins(:sensor)
                       .where({sensors: {user_id: user.id}})
                       .order('sensor_data.created_at desc')
                       .offset(offset)
                       .limit(limit)

    render json: @sensor_data
  end

  private

  def filtering_params(params)
    # allow only certain parameters to filter the sensor_data
    filtering_params = params.slice(:after, :before)
    filtering_params.delete_if do |key, value|
      next false unless value.present?

      # switch cases to handle different parameters and remove parameters that are invalid.
      case key
      when 'after', 'before'
        # remove parameter if Time.parse can't parse the value as a valid date.
        begin
          Time.parse value
        rescue ArgumentError
          next true
        end
      else
        # nothing yet
      end
      next false
    end
    filtering_params
  end

  def between(input, default = 0, min = 0, max = nil)
    begin
      input = input&.present? ? Integer(input) : default
      input = min unless input >= min
      input = max unless max == nil || input <= max
      return input
    rescue ArgumentError
      return default
    end
  end
end
