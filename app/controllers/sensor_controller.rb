class SensorController < ApplicationController
  before_action :set_sensor, only: %i[unclaim, set_state]
  before_action :validate_ownership, only: %i[unclaim, set_state]

  def get
    render json: Sensor.where(:user_id => current_user.id).order('id ASC').all
  end

  def claim
    @sensor = Sensor.find_or_create_by(id: params[:id])
    validate_ownership
    @sensor.update(user_id: current_user.id)
    render json: @sensor
  end

  def unclaim
    @sensor.update(user: nil)
    render json: @sensor
  end

  def set_state
    return render json: { error: 'no new state found' }, :status => :bad_request unless params[:state].present?
    begin
      state = Integer(params[:state])
    rescue
      return render json: { error: 'state is not an integer' }, :status => :bad_request
    end
    byte = [state].pack('C*')
    mqtt = MqttService.new
    mqtt.connect
    mqtt.send("iot/device/#{@sensor.id}/state", byte)
    mqtt.close
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_sensor
    @sensor = Sensor.find(params[:id])
  end

  def validate_ownership
    render json: { error: 'This sensor was claimed by someone else' }, :status => :forbidden if @sensor.user.present? && @sensor.user.id != current_user.id
  end
end