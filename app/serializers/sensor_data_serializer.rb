class SensorDataSerializer < ActiveModel::Serializer
  attributes :id, :sensor_id, :light, :state, :created_at, :updated_at
end
