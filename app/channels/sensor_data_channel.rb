# frozen_string_literal: true

class SensorDataChannel < ApplicationCable::Channel
  def subscribed
    user_id = @connection.current_user.id
    stream_from "sensor_data_#{user_id}"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

end
