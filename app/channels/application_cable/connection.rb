module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user

    def connect
      @current_user = find_verified_user
      logger.add_tags 'ActionCable', current_user.id
    end

    protected

    # @return [User]
    def find_verified_user
      unless request.params.key?(:token)
        reject_unauthorized_connection
      end

      token = request.params[:token]
      begin
        jwt = JWT.decode(token, Rails.application.secrets.secret_key_jwt, true, algorithm: 'HS256', verify_jti: true)[0]
      rescue
        return reject_unauthorized_connection
      end
      if (user = User.find(jwt['sub']))
        user
      else
        reject_unauthorized_connection
      end
    end
  end
end
