class Sensor < ApplicationRecord
  belongs_to :user, optional: true
  has_many :sensor_data, :dependent => :destroy
end
