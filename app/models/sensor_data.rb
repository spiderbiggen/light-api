class SensorData < ApplicationRecord
  belongs_to :sensor
  include Filterable
  enum state: {auto: 'auto', closed: 'closed', open: 'open', force_closed: 'force_closed', force_open: 'force_open'}, _prefix: :state

  scope :filter_after, ->(timestamp) { where 'sensor_data.created_at >= ?', timestamp }
  scope :filter_before, ->(timestamp) { where 'sensor_data.created_at < ?', timestamp }

  after_commit { SensorDataRelayJob.perform_later(self) }
end
